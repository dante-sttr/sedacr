#' @title Harmonize Rasters
#'
#' @description This function takes a vector of rasters and several parameters
#' and returns a vector of harmonized rasters and optionally saves the outputs to disk.
#' They will all have the same projection, resolution, and extent.
#' The resulting rasters can then be used in further analysis or visualization.
#'
#' @param rasters user provided vector of paths on disk or in workspace to the set of
#' rasters to harmonize. Required.
#'
#' @param proj desired projection is determined by the user providing either
#' the exact name of one of input rasters, the index number of one of input rasters
#' in vector, or the EPSG number of the desired projection. The default is to use the
#' project of the first raster provided in the rasters parameter. Not required.
#'
#' @param proj_method if projection is necessary then a projection method must be
#' provided. The possible values are "nearest neighbor", which is most suitable when
#' working with discrete data, or "interpolate" which is suitable for continuous data.
#' The default value is "interpolate". Not required.
#'
#' @param cell_size cell size is determined by the user providing either
#' the exact name of one of input rasters, the numerical value of desired resolution,
#' or one of the keywords "finest" or "coarsest". The default is "coarsest". Not required.
#'
#' @param cell_agg_method when it is necessary to aggregate data in order to harmonize the
#' set of rasters, the cell_agg_method determines the basis for the aggregation. Possible
#' values are one of the keywords "sum", "mean", "max", or "min". The default value is "mean".
#' Not required.
#'
#' @param cell_disagg_method when it is necessary to disaggregate data in order to harmonize the
#' set of rasters, the cell_disagg_method determines the basis for the disaggregation. Possible
#' values are one of the keywords "interpolate", or "divide". The default value is "divide".
#' Not required.
#'
#' @param ext extent is determined by the user providing either
#' the exact name of one of input rasters, the index of one of input rasters,
#' a vector extent (xmin, xmax, ymin, ymax), an object of class Extent, or one of
#' the keywords "largest" or "smallest". The default is "largest". Not required.
#'
#' @param save_outputs binary parameter for user to save the harmonized output rasters.
#' The keywords are "TRUE" or "FALSE". The default is "FALSE". Not required.
#'
#' @param output_dir when save_outputs is set to "TRUE" users must provide a path to
#' a directory on disk where the outputs will be saved. Default is NULL
#' which will save the outputs to the same folder as the input rasters. Not required.
#'
#' @return object of class 'raster'
#' @export

harmonizeRasters <- function(rasters,
                      proj=1, proj_method='interpolate',
                      cell_size='coarsest', cell_agg_method='', cell_disagg_method='',
                      ext='largest',
                      save_outputs=FALSE, output_dir=''){

  # Create raster objects for manipulation
  raster_names <- sapply(rasters, basename)
  raster_objects <- sapply(rasters, raster::raster)
  raster_folders <- sapply(rasters, dirname)

  # Check projection
  if (!proj %in% raster_names & !(is.numeric(proj) & proj <= length(rasters)) & !(is.numeric(proj) & proj > 1000)){
    stop("Projection provided is not an allowed value (raster name, index of raster, EPSG number)")
  }

  # Check projection method
  if (!proj_method %in% c('interpolate', 'nearest neighbor')){
    stop("Projection method provided is not an allowed value ('interpolate', 'nearest neighbor')")
  }

  # Check cell size
  if (!cell_size %in% c('finest', 'coarsest') & !is.numeric(cell_size) & !cell_size %in% raster_names){
    stop("Cell size provided is not an allowed value (numeric value, raster name, 'finest', 'coarsest'")
  }

  # Check aggregation method
  if (!cell_agg_method %in% c("sum", "mean", "max", "min", '')){
    stop("Aggregation method provided is not an allowed value ('sum', 'mean', 'max', 'min')")
  }

  # Check disaggregation method
  if (!cell_disagg_method %in% c("interpolate", "divide", '')){
    stop("Disaggregation method provided is not an allowed value ('interpolate', 'divide')")
  }

  # Check extent
  if (!ext %in% c('largest','smallest',raster_names) & class(ext) !='Extent' & !(is.vector(ext) & length(ext) == 4) & !(is.numeric(ext) & length(ext) == 1 & ext < length(rasters))){
    stop("Extent is not an allowed value ('largest', 'smallest', an extent object, a vector extent, or one of the raster names or indices)")
  }

  # Check save parameter
  if (!save_outputs %in% c(TRUE, FALSE)){
    stop("Save option is not either True or False")
  }

  # Check output directory
  if (output_dir != '' & !dir.exists(output_dir)){
    stop("Provided output directory does not exist")
  }

  # Alter projection if needed
  projs <- sapply(raster_objects, raster::crs, USE.NAMES = FALSE, asText=TRUE)
  if (length(unique(projs)) > 1){

    # Find output projection
    if (is.element(proj, raster_names)){
      index <- which(raster_names == proj)
      out_proj <- raster::crs(raster_objects[[index]], USE.NAMES = FALSE, asText=TRUE)
    }else if (is.numeric(proj) & proj <= length(rasters)){
      out_proj <- raster::crs(raster_objects[[proj]], USE.NAMES = FALSE, asText=TRUE)
    }else if (is.numeric(proj) & proj > 1000){
      epsg <- rgdal::make_EPSG()
      epsg <- epsg[stats::complete.cases(epsg),]
      out_proj <- gsub("\\b \\+type=crs\\b", "", epsg$prj4[epsg$code == proj])
    }

    # Project
    to_project <- which(projs != out_proj)
    if (proj_method == 'interpolate'){proj_method <- 'bilinear'}
    if (proj_method == 'nearest neighbor'){proj_method <- 'ngb'}
    raster_objects_projected <- sapply(raster_objects[to_project], raster::projectRaster,
                                       crs=out_proj, method=proj_method)

    # Substitute raster objects for projected ones
    raster_objects[to_project] <- raster_objects_projected
  }

  # Alter cell size if needed
  ress <- sapply(raster_objects, raster::res)[1,]
  if (stats::var(ress) != 0) {

    # Find output cell size
    if (is.element(cell_size, raster_names)){
      index <- which(raster_names == cell_size)
      out_cell <- raster::res(raster_objects[[index]])[1]
    }else if (is.numeric(cell_size)){
      out_cell <- cell_size
    }else if (cell_size == 'finest'){
      out_cell <- min(ress)
    }else if (cell_size == 'coarsest'){
      out_cell <- max(ress)
    }

    # Which ones need changing?
    to_cell_agg <- which(ress < out_cell)
    to_cell_disagg <- which(ress > out_cell)
    target <- which(ress == out_cell)

    # Aggregate rasters
    if (length(to_cell_agg) > 0){
      if (cell_agg_method == ''){cell_agg_method <- 'mean'}
      warn <- 0
      for (i in to_cell_agg){
        fact <- (out_cell/ress)[i]
        if (as.integer(fact)==fact){
          raster_object_celled <- raster::aggregate(raster_objects[[i]], fact, fun=cell_agg_method)
        }else{
          warn <- warn + 1
          resampledRaster <- raster::raster(resolution=out_cell/floor(fact), crs=out_proj, ext=raster::extent(raster_objects[[i]]))
          raster_object_celled <- raster::resample(raster_objects[[i]], resampledRaster, method='bilinear')
          raster_object_celled <- raster::aggregate(raster_object_celled, floor(fact), fun=cell_agg_method)
        }
        # Replace elements with new raster
        raster_objects[[i]] <- raster_object_celled
      }
      if (warn > 0){
        warning('Raster cell sizes are not multiples of each other, therefore aggregation may produce unexpected values')
      }
    }

    # Disaggregate rasters
    if (length(to_cell_disagg) > 0){
      if (cell_disagg_method %in% c('divide','')){cell_disagg_method <- ''}
      if (cell_disagg_method == 'interpolate'){cell_disagg_method <- 'bilinear'}

      for (i in to_cell_disagg){
        fact <- (ress/out_cell)[i]
        if (as.integer(fact) == fact){
          raster_object_celled <- raster::disaggregate(raster_objects[[i]], fact, fun=cell_disagg_method)
        }else{
          if (length(target) > 0){
            resampledRaster <- raster_objects[[target[1]]]
            resampledRaster <- raster::extend(resampledRaster, y=raster::extent(raster_objects[[i]]))
            resampledRaster <- raster::crop(resampledRaster, y=raster::extent(raster_objects[[i]]))
          }else{
            inCols <- ncol(raster_objects[[i]])
            inRows <- nrow(raster_objects[[i]])
            resampledRaster <- raster::raster(ncol=inCols*fact, nrow=inRows*fact, crs=out_proj, ext=raster::extent(raster_objects[[i]]))
          }
          raster_object_celled <- raster::resample(raster_objects[[i]], resampledRaster, method=cell_disagg_method)
        }
        # Replace elements with new raster
        raster_objects[[i]] <- raster_object_celled
      }
    }
  }

  # Alter extent if needed
  exts <- sapply(raster_objects, raster::extent)
  if (length(unique(exts)) > 1){

    # Get current extents
    xmins <- sapply(raster_objects, raster::xmin, USE.NAMES = FALSE)
    xmaxs <- sapply(raster_objects, raster::xmax, USE.NAMES = FALSE)
    ymins <- sapply(raster_objects, raster::ymin, USE.NAMES = FALSE)
    ymaxs <- sapply(raster_objects, raster::ymax, USE.NAMES = FALSE)

    # Find output extent
    if (is.element(ext, raster_names)){
      index <- which(raster_names == ext)
      out_ext <- raster::extent(raster_objects[[index]])
    }else if (is.numeric(ext) & length(ext) == 1 & ext < length(rasters)){
      out_ext <- raster::extent(raster_objects[[ext]])
    }else if (class(ext)=='Extent'){
      out_ext <- ext
    }else if (is.vector(ext) & length(ext) == 4){
      out_ext <- raster::extent(ext)
    }else if (ext == 'largest'){
      xmin <- xmins[which.max(abs(xmins))]
      xmax <- xmaxs[which.max(abs(xmaxs))]
      ymin <- ymins[which.max(abs(ymins))]
      ymax <- ymaxs[which.max(abs(ymaxs))]
      out_ext <- raster::extent(c(xmin, xmax, ymin, ymax))
    }else if (ext == 'smallest'){
      xmin <- xmins[which.min(abs(xmins))]
      xmax <- xmaxs[which.min(abs(xmaxs))]
      ymin <- ymins[which.min(abs(ymins))]
      ymax <- ymaxs[which.min(abs(ymaxs))]
      out_ext <- raster::extent(c(xmin, xmax, ymin, ymax))
    }

    # Which ones need changing?
    to_ext <- which(!sapply(exts, identical, y=out_ext))
    if (length(to_ext) > 0){

      # Change extent
      # Substitute raster objects for ones with correct extent
      raster_objects_extended <- lapply(raster_objects[to_ext], FUN=raster::extend, y=out_ext)
      raster_objects[to_ext] <- raster_objects_extended

      raster_objects_cropped <- lapply(raster_objects[to_ext], FUN=raster::crop, y=out_ext)
      raster_objects[to_ext] <- raster_objects_cropped
    }
  }

  if (save_outputs == TRUE){
    for (i in 1:length(raster_objects)){
      if (output_dir == ''){
        raster_file <- paste0(raster_folders[i], "/", strsplit(raster_names[i], split="\\.")[[1]][1], "_harmonized.tif")
      }else{
          raster_file <- paste0(output_dir, "/", strsplit(raster_names[i], split="\\.")[[1]][1], "_harmonized.tif")}
      raster::writeRaster(raster_objects[[i]], raster_file, format="GTiff", options="COMPRESS=LZW", overwrite=TRUE)
    }
  }

  # Return rasters
  return(raster_objects)
}


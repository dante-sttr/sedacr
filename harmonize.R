# Jane Mills
# 2/25/2020
# DANTE
# raster harmonization functions

# rasters: vector of raster files

# proj: desired projection, either name/index of one raster or EPSG integer
# proj_method: ngb, bilinear

# cell_size: numerical cell size, name, highest, or lowest
# agg_method: text with options sum, mean, max, min, resample, downsample, etc

# ext: bounding box, name/index of one raster in vector, largest, smallest

# output: same vector of rasters but harmonized

harmonize <- function(rasters, 
                      proj=1, proj_method='bilinear', 
                      cell_size='largest', cell_agg_method='', cell_disagg_method='', 
                      ext='largest', save_outputs=FALSE){
  
  # Create raster objects for manipulation
  raster_names <- sapply(rasters, basename)
  raster_objects <- sapply(rasters, raster)
  raster_folders <- sapply(rasters, dirname)
  
  # Check projection
  if (!proj %in% raster_names & !(is.numeric(proj) & proj <= length(rasters)) & !(is.numeric(proj) & proj > 1000)){
    stop("Projection provided is one of allowed values (raster name, index of raster, EPSG number)")
  }
  
  # Check projection method
  if (!proj_method %in% c('bilinear', 'ngb')){
    stop("Projection method provided is not either 'ngb' or 'bilinear'")
  }
  
  # Check cell size
  if (!cell_size %in% c('largest', 'smallest') & !is.numeric(cell_size) & !cell_size %in% raster_names){
    stop("Cell size provided is not one of allowed values (numeric, raster name, 'largest', 'smallest'")
  }
  
  # Check aggregation method
  if (!cell_agg_method %in% c("sum", "mean", "max", "min", '')){
    stop("Aggregation method provided is not one of the allowed values ('sum','mean','max','min')")
  }
  
  # Check disaggregation method
  if (!cell_disagg_method %in% c("bilinear", "disaggregate", '')){
    stop("Disaggregation method provided is not one of the allowed values ('bilinear','disaggregate')")
  }
  
  # Check extent
  if (!ext %in% c('largest','smallest',raster_names) & class(ext) !='Extent' & !(is.vector(ext) & length(ext) == 4) & !(is.numeric(ext) & length(ext) == 1 & ext < length(rasters))){
    stop("Extent is not in one of allowed formats ('largest','smallest', an extent object, a vector extent, or one of the raster names or indices)")
  }
  
  # Check save parameter
  if (!save_outputs %in% c(TRUE, FALSE)){
    stop("Save option is not either True or False")
  }
  
  # Alter projection if needed
  projs <- sapply(raster_objects, crs, USE.NAMES = FALSE, asText=TRUE)
  if (length(unique(projs)) > 1){
    
    # Find output projection
    if (is.element(proj, raster_names)){
      index <- which(raster_names == proj)
      out_proj <- raster::crs(raster_objects[[index]], USE.NAMES = FALSE, asText=TRUE)
    }else if (is.numeric(proj) & proj <= length(rasters)){
      out_proj <- raster::crs(raster_objects[[proj]], USE.NAMES = FALSE, asText=TRUE)
    }else if (is.numeric(proj) & proj > 1000){
      epsg <- rgdal::make_EPSG()
      epsg <- epsg[complete.cases(epsg),]
      out_proj <- gsub("\\b \\+type=crs\\b", "", epsg$prj4[epsg$code == proj])
    }
    
    # Project
    to_project <- which(projs != out_proj)
    raster_objects_projected <- sapply(raster_objects[to_project], raster::projectRaster, 
                                       crs=out_proj, method=proj_method)
    
    # Substitute raster objects for projected ones
    raster_objects[to_project] <- raster_objects_projected
  }
  
  # Alter cell size if needed
  ress <- sapply(raster_objects, raster::res)[1,]
  if (var(ress) != 0) {
    
    # Find output cell size
    if (is.element(cell_size, raster_names)){
      index <- which(raster_names == cell_size)
      out_cell <- raster::res(raster_objects[[index]])[1]
    }else if (is.numeric(cell_size)){
      out_cell <- cell_size
    }else if (cell_size == 'smallest'){
      out_cell <- min(ress)
    }else if (cell_size == 'largest'){
      out_cell <- max(ress)
    }
    
    # Which ones need changing?
    to_cell_agg <- which(ress < out_cell)
    to_cell_disagg <- which(ress > out_cell)
    target <- which(ress == out_cell)
    
    # Aggregate rasters
    if (length(to_cell_agg) > 0){
      if (cell_agg_method == ''){cell_agg_method <- 'mean'}
      warn <- 0
      for (i in to_cell_agg){
        fact <- (out_cell/ress)[i]
        if (as.integer(fact)==fact){
          raster_object_celled <- raster::aggregate(raster_objects[[i]], fact, fun=cell_agg_method)
        }else{
          warn <- warn + 1
          resampledRaster <- raster::raster(resolution=out_cell/floor(fact), crs=out_proj, ext=raster::extent(raster_objects[[i]]))
          raster_object_celled <- raster::resample(raster_objects[[i]], resampledRaster, method='bilinear')
          raster_object_celled <- raster::aggregate(raster_object_celled, floor(fact), fun=cell_agg_method)
        }
        # Replace elements with new raster
        raster_objects[[i]] <- raster_object_celled
      }
      if (warn > 0){
        warning('Raster cell sizes are not multiples of each other, therefore aggregation may produce unexpected values')
      }
    }
    
    # Disaggregate rasters
    if (length(to_cell_disagg) > 0){
      if (cell_disagg_method %in% c('disaggregate','')){cell_disagg_method <- ''}
      
      for (i in to_cell_disagg){
        fact <- (ress/out_cell)[i]
        if (as.integer(fact) == fact){
          raster_object_celled <- raster::disaggregate(raster_objects[[i]], fact, fun=cell_disagg_method)
        }else{
          if (length(target) > 0){
            resampledRaster <- raster_objects[[target[1]]]
            resampledRaster <- raster::extend(resampledRaster, y=raster::extent(raster_objects[[i]]))
            resampledRaster <- raster::crop(resampledRaster, y=raster::extent(raster_objects[[i]]))
          }else{
            inCols <- ncol(raster_objects[[i]])
            inRows <- nrow(raster_objects[[i]])
            resampledRaster <- raster::raster(ncol=inCols*fact, nrow=inRows*fact, crs=out_proj, ext=raster::extent(raster_objects[[i]]))
          }
          raster_object_celled <- raster::resample(raster_objects[[i]], resampledRaster, method=cell_disagg_method)
        }
        # Replace elements with new raster
        raster_objects[[i]] <- raster_object_celled
      }
    }
  }
  
  # Alter extent if needed
  exts <- sapply(raster_objects, raster::extent)
  if (length(unique(exts)) > 1){
    # Get current extents
    xmins <- sapply(raster_objects, raster::xmin, USE.NAMES = FALSE)
    xmaxs <- sapply(raster_objects, raster::xmax, USE.NAMES = FALSE)
    ymins <- sapply(raster_objects, raster::ymin, USE.NAMES = FALSE)
    ymaxs <- sapply(raster_objects, raster::ymax, USE.NAMES = FALSE)
    # Find output extent
    if (is.element(ext, raster_names)){
      index <- which(raster_names == ext)
      out_ext <- raster::extent(raster_objects[[index]])
    }else if (is.numeric(ext) & length(ext) == 1 & ext < length(rasters)){
      out_ext <- raster::extent(raster_objects[[ext]])
    }else if (class(ext)=='Extent'){
      out_ext <- ext
    }else if (is.vector(ext) & length(ext) == 4){
      out_ext <- raster::extent(ext)
    }else if (ext == 'largest'){
      xmin <- xmins[which.max(abs(xmins))]
      xmax <- xmaxs[which.max(abs(xmaxs))]
      ymin <- ymins[which.max(abs(ymins))]
      ymax <- ymaxs[which.max(abs(ymaxs))]
      out_ext <- raster::extent(c(xmin, xmax, ymin, ymax))
    }else if (ext == 'smallest'){
      xmin <- xmins[which.min(abs(xmins))]
      xmax <- xmaxs[which.min(abs(xmaxs))]
      ymin <- ymins[which.min(abs(ymins))]
      ymax <- ymaxs[which.min(abs(ymaxs))]
      out_ext <- raster::extent(c(xmin, xmax, ymin, ymax))
    }
    # Which ones need changing?
    to_ext <- which(!sapply(exts, identical, y=out_ext))
    if (length(to_ext) > 0){
      # Change extent
      # Substitute raster objects for ones with correct extent
      raster_objects_extended <- lapply(raster_objects[to_ext], FUN=raster::extend, y=out_ext)
      raster_objects[to_ext] <- raster_objects_extended
      raster_objects_cropped <- lapply(raster_objects[to_ext], FUN=raster::crop, y=out_ext)
      raster_objects[to_ext] <- raster_objects_cropped
    }
  }
  if (save_outputs == TRUE){
    for (i in 1:length(raster_objects)){
      raster_file <- paste0(raster_folders[i], "/", strsplit(raster_names[i], split="\\.")[[1]][1], "_harmonized.tif")
      raster::writeRaster(raster_objects[[i]], raster_file, format="GTiff", options="COMPRESS=LZW", overwrite=TRUE)
    }
  }
  # Return rasters
  return(raster_objects)
}

# Testing
setwd("~/Work/DANTE")
tifs<- list.files("D:/dante/sedacr/test2/rasters",pattern='tif$', full.names = TRUE)
rasters<-append(tifs,list.files("D:/dante/sedacr/test2/rasters",pattern='g$', full.names = TRUE))
harmonized_rasters <- harmonize(rasters, cell_size='smallest', ext='smallest', cell_agg_method = 'sum', save_outputs = TRUE)



<!-- README.md is generated from README.Rmd. Please edit that file -->

# sedacr <img src='man/figures/logo.png' align="right" height="150" />

<!-- badges: start -->
<!-- badges: end -->

The goal of **sedacr** is to enable programmatic access to data from the
NASA Socioeconomic Data and Applications (SEDAC) data repository.

## Installation

You can install the released version of sedacR from
[GitLab](https://gitlab.com/) with:

``` r
devtools::install_gitlab("dante-sttr/sedacr", dependencies=TRUE)
```

### Installation With Windows

To install R packages over Git on a Windows system, you must install
Rtools first. The latest version of Rtools is available
[here](https://cran.r-project.org/bin/windows/Rtools/). Furthermore, you
may experience difficulty installing R packages over Git if you utilize
a Windows machine on a network with Active Directory or shared network
drives. To enable proper package installation under these circumstances
please follow [this
guide](https://dante-sttr.gitlab.io/dante-vignettes/windows-pkg-inst/windows-pkg-inst.html).

## Example

Access to NASA SEDAC data requires that users have registered an account
with NASA Earthdata see <https://urs.earthdata.nasa.gov/>

Once an account is created, it can be authenticated by creating a CURL
handle with the `authenticateSEDACURS` function.

``` r
myAuth<-authenticateSEDACURS(working_directory = getwd(), username = "Some User", password = "Some Password")
```

Users can request information about available data with the
`requestSEDACRepositoryInfo` function

``` r
repository<-requestSEDACRepositoryInfo()
data_inventory<-repository$data_inventory
data_downloads<-repository$data_downloads
View(data_inventory)
```

Users can explore the full set of data sets in the SEDAC repository by
reviewing their titles and abstracts in the data\_inventory data frame.
The data\_downloads data frame includes the full list of downloadable
zip files for each data set.

The data descriptor is captured in the row name of both. After deciding
which data to download, the data descriptor (row name) can be provided
as a parameter to the `downloadSEDACGranules` function. Please note that
these data will only work when myAuth is parameterized with your
Earthdata username and password in the CURL handle returned by the
`authenticateSEDACURS` function

``` r
# user selects a row name corresponding to the data set they would like to download
input_granule_data<-"aglands-croplands-2000"
download_vector<-as.data.frame(data_downloads[input_granule_data,"GRANULES"],col.names=list("GRANULES"))$GRANULES
results<-sapply(download_vector,downloadSEDACGranules,working_directory=getwd(),handle=myAuth)
```
